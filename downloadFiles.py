import base64
import os
from time import sleep
links = 'links'
lnks = sorted(os.listdir(links))
for lnk in lnks:
  fol = lnk.replace('.txt', '')
  os.system('mkdir -p files/%s' % fol)
  with open('links/%s' % lnk) as urlfile:
    urlArr = [each.replace('\n', '') for each in urlfile.readlines()]
  downFiles = os.listdir('files/%s' % fol)
  for url in urlArr:
    hashval = (base64.b64encode(url.encode())).decode()
    if hashval not in downFiles:
      os.system('wget %s -O files/%s/download; mv files/%s/download files/%s/%s' % (url, fol, fol, fol, hashval))
      sleep(1)